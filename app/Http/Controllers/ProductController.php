<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function create(Request $request)
    {
        $nama=$request->nama;
        $deskripsi=$request->deskripsi;
        $photo=$request->photo;
        $type=$request->type;
        $quant=$request->quant;
        $harga=$request->harga;

        $simpan=Product::create([
            'name'=>$nama,
            'description'=>$deskripsi,
            'photo'=>$photo,
            'type'=>$type,
            'quantity'=>$quant,
            'price'=>$harga,
        ]);
        if($simpan){
            $print=[
                'code'=>200,
                'msg'=>'Berhasil Disimpan!'
            ];
        }else{
            $print=[
                'code'=>201,
                'msg'=>'Gagal Disimpan!'
            ];
        }
        return response()->json($print);
    }
    public function show()
    {
        $produk=Product::all();
        return response()->json([
            'produk'=>$produk,
        ], 200);
    }
    public function productId($id)
    {
        // $data=Product::where('id',$id)->get();
        $product=Product::find($id);
        return response()->json([
            'product'=>$product
        ]);

    }
    public function update(Request $request,$id)
    {
        $simpan=Product::where('id',$id)->update([
            'name'=>$request->nama,
            'description'=>$request->deskripsi,
            'type'=>$request->type,
            'quantity'=>$request->quant,
            'price'=>$request->harga,
        ]);
        if($simpan){
            $print=[
                'code'=>200,
                'msg'=>'Berhasil Disimpan!'
            ];
        }else{
            $print=[
                'code'=>201,
                'msg'=>'Gagal Disimpan!'
            ];
        }
        return response()->json($print);
    }
    public function hapus($id)
    {
        $hapus=Product::where('id',$id)->delete();
        if($hapus){
            $print=[
                'code'=>200,
                'msg'=>'Berhasil Dihapus!'
            ];
        }else{
            $print=[
                'code'=>201,
                'msg'=>'Gagal Dihapus!'
            ];
        }
        return response()->json($print);
    }
}
