import React,{useEffect,useState} from "react";
// aggrid

import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { DataGrid,GridColumn } from "rc-easyui";

const Index=()=>{
    const navigate=useNavigate()
    const [products,setProduks]=useState([])
    const [pagin,setPagin]=useState([])
    const newProduct=()=>{
        navigate('/product/new')
    }
    useEffect(()=>{
        getproduct()
        // setTabel()
    },[])
    const getproduct=async()=>{
        await axios.get('/product/show').then(({data})=>{
            // console.log('data',data);
            setProduks(data.produk)
            setPagin({
                total: 0,
                pageSize: 20,
                data: data.produk,
                pagePosition: "bottom",
                pageOptions: {
                    layout: ['list', 'sep', 'first', 'prev', 'next', 'last', 'sep', 'refresh', 'sep', 'manual', 'info']
                },
            })
        })
    }
    const EditProduct=(id)=>{
        navigate('/product/edit/'+id);
        // alert(id)
    }
    const hapusProduct=async(kode)=>{
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                axios.get('/product/hapus/'+kode).then(({data})=>{
                    var code=data.code;
                    var msg=data.msg;
                    if(code=="200"){
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        getproduct();
                    }else{
                        toast.fire({
                            icon:'error',
                            title:msg,
                        })
                    }
                })
                
            }
        })
    }
    return (    
       <div className="container mt-5">
            <div className="row">
                <div className="col-12 col-md-12 col-lg-12">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-title">Data Product</div>
                            <div className="d-flex ms-auto">
                                <button onClick={()=>newProduct()} className="btn btn-primary">Tambah produk</button>
                            </div>
                        </div>
                        <div className="card-body">
                        <DataGrid filterable data={products} style={{height:600}} pagination {...pagin}>
                        
                            <GridColumn field="rn" title="No" align="center" width="30px"
                            cellCss="datagrid-td-rownumber" sortOrder="asc"
                            render={({rowIndex}) => (
                            <span>{rowIndex+1}</span>
                            )}
                            />
                            <GridColumn sortable="true" field="name" title="Nama"></GridColumn>
                            <GridColumn sortable="true" field="description" title="Deskripsi" align="right"></GridColumn>
                            <GridColumn sortable="true" field="type" title="type" align="right"></GridColumn>
                            <GridColumn sortable="true" field="price" title="Harga" width="30%"></GridColumn>
                            <GridColumn sortable="true" field="quantity" title="Stok" align="center"></GridColumn>
                            <GridColumn field="rn" title="Opsi" align="center" width="10%"
                            render={({rowIndex}) => (
                            <span>
                                <a href="#" onClick={()=>EditProduct(products[rowIndex].id)} className="mr-2"><i className="fa fa-edit"></i>  </a>
                                <a href="#" onClick={()=>hapusProduct(products[rowIndex].id)} ><i className="fa fa-trash"></i></a>
                            </span>
                            )}
                            />
                        </DataGrid>
                            <hr />
                            {/* <button className="btn btn-primary btn-sm"></button> */}
                            {/* <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Deskripsi</th>
                                        <th>type</th>
                                        <th>Harga</th>
                                        <th>Stok</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        products.length > 0 && (
                                            products.map((item,key)=>(
                                                <tr key={key}>
                                                    <td>{key+1}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.description}</td>
                                                    <td>{item.type}</td>
                                                    <td>{item.quantity}</td>
                                                    <td>{item.price}</td>
                                                    <td>
                                                        <button className="btn btn-sm btn-info mr-2" onClick={()=>EditProduct(item.id)}><i className="fa fa-edit"></i></button>
                                                        <button className="btn btn-sm btn-danger mr-2" onClick={()=>hapusProduct(item.id)}><i className="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                            ))
                                        )
                                    }
                                    
                                </tbody>
                            </table> */}
                        </div>
                    </div>
                </div>
                {/* <div className="col-12 col-md-4 col-lg-4">
                    .car
                </div> */}
            </div>
       </div>
    )
}

export default Index