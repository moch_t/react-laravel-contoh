import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const NewProduct=()=>{

    const navigate=useNavigate()
    
    const Home=()=>{
        navigate('/')
    }
    const [nama,setnama]=useState('');
    const [deskripsi,setDeskripsi]=useState('');
    const [type,setType]=useState('');
    const [quant,setQuant]=useState('');
    const [harga,setHarga]=useState('');

    const createProduct=async(e)=>{
        e.preventDefault()
        const formData=new FormData()
        formData.append('nama',nama)
        formData.append('deskripsi',deskripsi)
        formData.append('type',type)
        formData.append('quant',quant)
        formData.append('harga',harga)

        await axios.post('/product/create',formData).then(({data})=>{
            console.log(data);
            var code=data.code;
            var msg=data.msg;
            if(code=="200"){
                toast.fire({
                    icon:'success',
                    title:msg,
                })
                navigate('/')
            }else{
                toast.fire({
                    icon:'success',
                    title:msg,
                })
            }
        }).catch(({e})=>{
            toast.fire({
                icon:'success',
                title:'Gagal Disimpan',
            })
        })

    }
    return(
        <div className="container">
            <div className="mt-5">
                <div className="card">
                    <div className="card-header">
                        <div className="card-title">Tambah Produk</div>
                        <div className="d-fleex ms-auto"> 
                            <button onClick={()=>{Home()}} className="btn btn-danger"><i className="fa fa-arrow-left"></i> back</button>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <label htmlFor="">Barang</label>
                                    <input type="text" className="form-control" value={nama} placeholder="Isikan barang" onChange={(event)=>{setnama(event.target.value)}} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Deskripsi</label>
                                    <input type="text" className="form-control" value={deskripsi} placeholder="Isikan Deskripsi" onChange={(event)=>{setDeskripsi(event.target.value)}} />
                                </div>
                            </div>
                            <div className="col-12 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <label htmlFor="">Produk Type</label>
                                    <input type="text" className="form-control" value={type} onChange={(event)=>{setType(event.target.value)}} placeholder="Isikan type" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Stok</label>
                                    <input type="text" className="form-control" value={quant} placeholder="Isikan Stok" onChange={(event)=>{setQuant(event.target.value)}} />
                                </div>
                            </div>
                            <div className="col-12 col-md-4 col-lg-4">
                                <div className="form-group">
                                    <label htmlFor="">Harga</label>
                                    <input type="number" className="form-control" value={harga} placeholder="Isikan Gambar" onChange={(event)=>{setHarga(event.target.value)}} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="row">
                            <div className="col-12 col-lg-10"></div>
                            <div className="col-12 col-lg-2">
                                <button className="btn btn-primary text-right" onClick={(event)=>{createProduct(event)}}>Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewProduct