<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('product')->group(function(){
    Route::post('create',[ProductController::class,'create']);
    Route::get('show',[ProductController::class,'show']);
    Route::get('get-product-id/{id}',[ProductController::class,'productId']);
    Route::post('/update/{id}',[ProductController::class,'update']);
    Route::get('/hapus/{id}',[ProductController::class,'hapus']);
});